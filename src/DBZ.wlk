class Guerrero {

/* Los guerreros tienen un potencial ofensivo,
 * con el cual atacan, un nivel de experiencia
 * y un nivel de energía, que llega a cero cuando mueren. */

	var ofensa
	var experiencia
	var energia
	var energiaTotal
	var traje

	constructor(_ofensa, _experiencia, _energia, _traje) {
		ofensa = _ofensa
		experiencia = _experiencia
		energia = _energia
		energiaTotal = _energia
		traje = _traje
	}
	method ofensa() {
		return ofensa
	}
	method experiencia() {
		return experiencia
	}
	method energia() {
		return energia
	}

/* Cuando un guerrero ataca al otro,
 * lo hace con todo su potencial ofensivo. */

	method atacar(atacado) {
		atacado.serAtacado(self.ofensa() * 0.1)
	}

/* Al recibir un ataque, la energía del guerrero atacado
 * disminuye en un valor igual al 10% del potencial ofensivo del atacante.*/

	method recibirDanio(danio) {
		energia -= danio - traje.disminuirDanio(danio)
	}
	method serAtacado(danio) {
		traje.desgastarse()
		self.recibirDanio(danio)
		experiencia += traje.bonusExperiencia()
	}

/* Cuando un guerrero come una semilla del ermitaño
 * su nivel de energía es restaurado al original.*/

	method comerSemilla() {
		energia = energiaTotal
	}
	method cuantasPiezas(){
		return traje.cuantasPiezas()
	}
}

/* Los trajes comunes disminuyen el daño recibido en un
 * porcentaje que depende de cada traje.*/

class Traje {
	var proteccion
	var bonus
	var desgaste = 0
	constructor(_proteccion) {
		proteccion = _proteccion
		bonus = 1
	}
	method disminuirDanio(danioTotal) {
		return if (self.estaGastado()) 0 else danioTotal * proteccion
	}
	method bonusExperiencia() {
		return bonus
	}

/* Todos los trajes tienen un nivel de desgaste,
 * cada vez que un traje es usado,
 * este nivel aumenta en 5 unidades.*/

	method desgastarse() {
		desgaste += 5
	}

/* Al llegar a las 100 unidades,
 * el traje está gastado y no puede usarse más.*/

	method estaGastado() {
		return desgaste >= 100
	}
	method cuantasPiezas(){
		return 1
	}
}

/* Los trajes de entrenamiento no disminuyen el daño recibido,
 * pero aumentan la experiencia del guerrero al doble de lo
 * normal mientras lo tiene puesto.*/

class TrajeDeEntrenamiento inherits Traje {
	constructor(_bonus)  = super(0) {
		bonus = _bonus
	}
	method modificarBonus(_bonus) {
		bonus = _bonus
	}
}

/* Los trajes modularizados están conformados por un conjunto de piezas.
 * Cada pieza tiene un porcentaje de resistencia y un valor de desgaste,
 * y se la considera "gastada" cuando tiene un desgaste igual o mayor a 20 unidades.*/

class TrajeModularizado inherits Traje {
	var piezas
	constructor(_piezas)  = super(0) {
		piezas = _piezas
	}

/* Al recibir un ataque, el daño disminuye en
 * una cantidad de unidades igual a la sumatoria
 * de las resistencias de cada pieza que no esté gastada. */

	override method disminuirDanio(_) {
		return self.piezasNoGastadas().sum(
			{ pieza => pieza.resistencia() }
		)
	}

/* Los trajes modularizados aumentan la experiencia
 * en base al porcentaje de piezas que tenga que no estén gastadas.*/

 	method piezasNoGastadas() {
		return piezas.filter(
			{ pieza => pieza.estaGastada().negate() }
		)
	}
	override method bonusExperiencia() {
		return self.piezasNoGastadas().size() / piezas.size()
	}

/* Un traje Modularizado está gastado
 * si todas sus piezas están gastadas.*/

	override method estaGastado() {
 		return piezas.all({pieza => pieza.estaGastada()})
 	}
	override method cuantasPiezas(){
		return piezas.size()
	}
}

class Pieza {
	var resistencia
	var desgaste
	constructor(_resistencia, _desgaste) {
		resistencia = _resistencia
		desgaste = _desgaste
	}
	method resistencia() {
		return resistencia
	}
	method desgaste() {
		return desgaste
	}
	method estaGastada() {
		return desgaste >= 20
	}
}

class Saiyan inherits Guerrero {
	var nivel = 0
	constructor(_ofensa, _experiencia, _energia, _traje) = super(_ofensa, _experiencia, _energia, _traje)

	method convertirseEnSuperSaiyan() {
		if (nivel >= 0 and nivel < 3) {
			nivel += 1
		}
	}

/* Cuando un Saiyan se convierte en Super Saiyan
 * su poder de ataque aumenta en un 50%.*/

	override method ofensa() {
		return if (nivel > 0) ofensa * 1.5 else ofensa
	}

/* Cuando se come una semilla del ermitaño
 * su poder de ataque aumenta un 5% del original.*/

	override method comerSemilla() {
		super()
		ofensa += ofensa * 0.05
	}

/* Un Saiyan puede volver a su estado original
 * cuando desee o su nivel de energía esté por debajo
 * de un 1% del original.*/

	override method recibirDanio(danio) {
		energia -= danio - traje.disminuirDanio(danio) - self.disminuirDanio(danio)
		if ((nivel != 0) and (energia < energiaTotal * 0.01)) {
			self.volverAlEstadoOriginal()
		}
	}

/* La energía que le quita un golpe es dependiente
 * del nivel en que se encuentra.*/

	method disminuirDanio(danio) {
		return if (nivel == 1) danio * 0.05
			else if (nivel == 2) danio * 0.07
			else if (nivel == 3) danio * 0.15
			else 0
	}
	method volverAlEstadoOriginal() {
		nivel = 0
	}
	
	method nivel() {
		return nivel
	}
}

object torneo {
	var jugadores = []
	method agregarJugador(jugador) {
		jugadores.add(jugador)
	}
	// Se seleccionarán los 16 peleadores con más poder de ataque
	method powerIsBest() {
		return jugadores.sortBy(
			{jugador1, jugador2 => jugador1.ofensa() > jugador2.ofensa()}
		).take(16)
	}
	// Se seleccionarán los 16 peleadores con más elementos en sus trajes
	method funny() {
		return jugadores.sortBy(
			{jugador1, jugador2 => jugador1.cuantasPiezas() > jugador2.cuantasPiezas()}
		).take(16)
	}
	// Se seleccionarán 16 peleadores al azar
	method surprise() {
		return jugadores.reverse().take(16)
	}
}