import DBZ.*

object fixture{
	const sinTraje = new Traje(0)
	const pechera = new Pieza(4,0)
	const casco = new Pieza(1,0)
	const piezaRota = new Pieza(2,20)
	const piezaSana = new Pieza(2,2)
	
	
	method dameAGoku(traje){
		return new Saiyan (100, 0, 6000, traje)
	}
	
	method dameAGokuPelado(){
		return new Saiyan (100, 0, 6000, sinTraje)
	}
	
	method dameAGokuBasico(){
		return new Guerrero (100, 0, 6000, sinTraje)
	}
	
	method dameAVegeta(traje){
		return new Saiyan (95, 0, 5000, traje)
	}
	
	method dameGuerrero(traje){
		return new Guerrero (50, 0, 4000, traje)
	}
	
	method dameGuerreroSinTraje(){
		return new Guerrero (50, 0, 4000, sinTraje)
	}
	
	method dameGuerreroPulenta(){
		return new Guerrero (200, 0, 2500, sinTraje)
	}
	
	method dameGuerreroDevastador(){
		return new Guerrero (200000, 0, 2500, sinTraje)
	}
	
	method dameTrajeModularizadoBasico(){
		return new TrajeModularizado ([pechera, casco])
	}
	
	method dameTrajeTodoRoto(){
		return new TrajeModularizado ([piezaRota, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota])
	}
	
	method dameTraje0KM(){
		return new TrajeModularizado ([piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaSana])
	}
	
	method dameTrajeMedioPelo(){
		return new TrajeModularizado ([piezaSana, piezaSana, piezaSana, piezaSana, piezaSana, piezaRota, piezaRota, piezaRota, piezaRota, piezaRota])
	}
	
	method dameTrajeCasiRoto(){
		return new TrajeModularizado ([piezaSana, piezaRota, piezaRota, piezaRota])
	}
}