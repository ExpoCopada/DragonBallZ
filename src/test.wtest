import fixture.*
import DBZ.*

test "Energia inicial de Goku es 6000 (trivial)" {
	const sinTraje = new Traje(0)
	const goku = fixture.dameAGoku(sinTraje)
	assert.equals(6000, goku.energia())
}

test "Si un guerrero ataca a otro 5 veces, el atacado aumenta su nivel de experiencia en 5" {
	const guerrero = fixture.dameGuerreroSinTraje()
	const otroGuerrero = fixture.dameGuerreroSinTraje()
	5.times{guerrero.atacar(otroGuerrero)}
	assert.equals(5, otroGuerrero.experiencia())
}

test "Si un guerrero con 50 de ofensa ataca 10 veces a otro con 4000 de energia y se come una semilla, restaura su energia" {
	const guerrero = fixture.dameGuerreroSinTraje()
	const otroGuerrero = fixture.dameGuerreroSinTraje()
	10.times{guerrero.atacar(otroGuerrero)}
	otroGuerrero.comerSemilla()
	assert.equals(4000, otroGuerrero.energia())
}

test "Si Goku ataca a Vegeta, Vegeta queda con 4990 de energia si no tiene traje" {
	const sinTraje = new Traje(0)
	const goku = fixture.dameAGoku(sinTraje)
	const vegeta = fixture.dameAVegeta(sinTraje)
	goku.atacar(vegeta)
	assert.equals(4990, vegeta.energia())
}

test "Si Goku ataca a Vegeta, y este tiene un traje de porcentaje 30, queda con 4993 de energia" {
	const trajeComunPtje3 = new Traje(0.3)
	const goku = fixture.dameAGoku(trajeComunPtje3)
	const vegeta = fixture.dameAVegeta(trajeComunPtje3)
	goku.atacar(vegeta)
	assert.equals(4993, vegeta.energia())
}

test "Si Goku tiene puesto un traje de entrenamiento y es atacado 5 veces, aumenta su experiencia en 10" {
	const trajeEntrenamiento = new TrajeDeEntrenamiento(2)
	const goku = fixture.dameAGoku(trajeEntrenamiento)
	const guerrero = fixture.dameGuerreroSinTraje()
	5.times{guerrero.atacar(goku)}
	assert.equals(10, goku.experiencia())
}

test "Si Goku tiene traje y es atacado 20 veces por otro guerrero, el traje se gasta" {
	const trajeComun5 = new Traje(5)
	const goku = fixture.dameAGoku(trajeComun5)
	const guerrero = fixture.dameGuerreroSinTraje()
	20.times{guerrero.atacar(goku)}
	assert.that(trajeComun5.estaGastado())
}

test "Si Vegeta lleva traje modularizado con piezas con resistencias 1 y 4, su energia al ser atacado por Goku sera 4995" {
	const trajeModularizado = fixture.dameTrajeModularizadoBasico()
	const vegeta = fixture.dameAVegeta(trajeModularizado)
	const goku = fixture.dameAGokuBasico()
	goku.atacar(vegeta)
	assert.equals(4995, vegeta.energia())
}

test "Un traje modularizado sano aumenta la experiencia en 100%" {
	const trajeSano = fixture.dameTraje0KM()
	assert.equals(1, trajeSano.bonusExperiencia())
}

test "Un traje modularizado con piezas mitad sanas y mitad rotas aumenta la experiencia en 50%" {
	const trajeMedioMedio = fixture.dameTrajeMedioPelo()
	assert.equals(0.5, trajeMedioMedio.bonusExperiencia())
}

test "Un traje modularizado con todas sus piezas rotas esta desgastado" {
	const trajeRoto = fixture.dameTrajeTodoRoto()
	assert.that(trajeRoto.estaGastado())
}

test "Un traje modularizado con con todas sus piezas rotas menos una no esta gastado" {
	const trajeMuyDestrozado = fixture.dameTrajeCasiRoto()
	assert.notThat(trajeMuyDestrozado.estaGastado())
}

test "Goku transformado en Super Saiyan Fase 1, aumenta su ofensa en 50 unidades, en total 150"{
	const goku = fixture.dameAGokuPelado()
	goku.cambiarNivel(3)
	assert.equals(150, goku.ofensa())
}

test "Si Goku es SSJ3, no tiene traje y es atacado por un guerrero con ofensa 200 y es SSJ3, el daño recibido es de 17"{
	const goku = fixture.dameAGokuPelado()
	const cell = fixture.dameGuerreroPulenta()
	goku.cambiarNivel(3)
	cell.atacar(goku)
	assert.equals(5983, goku.energia())
}

test "Si Goku se come una semillas y es SuperSaiyan, su ofensa será de 157.5"{
	const goku = fixture.dameAGokuPelado()
	goku.cambiarNivel(3)
	goku.comerSemilla()
	assert.equals(157.5, goku.ofensa())
}

test "Si Goku es atacado muy ferozmente, pierde su estado de SSJ"{
	const goku = fixture.dameAGokuPelado()
	const majinBu = fixture.dameGuerreroDevastador()
	goku.cambiarNivel(1)
	majinBu.atacar(goku)
	assert.equals(0, goku.nivel())	
}

test "Si hay un guerrero que no tiene muy baja ofensa, no es elegido para el torneo PowerIsBest"{
	const sinTraje = new Traje (0)
	const mrSatan = new Guerrero (1, 0, 100, sinTraje)
	const guerreroZ = new Guerrero (100, 0, 5000, sinTraje)
	16.times{torneo.agregarJugador(guerreroZ)}
	torneo.agregarJugador(mrSatan)
	var listaGuerreros = torneo.powerIsBest()
	assert.notThat(listaGuerreros.contains(mrSatan))
}

test "Si todos los guerreros tienen trajes modularizados, menos uno, este ultimo no entra en el torneo funny"{
	const trajeModularizado = fixture.dameTrajeModularizadoBasico()
	const trajeComun = new Traje (0)
	const krilin = new Guerrero(50, 0, 2500, trajeComun)
	const guerreroArmado = new Guerrero(75, 0, 5000, trajeModularizado)
	16.times{torneo.agregarJugador(guerreroArmado)}
	torneo.agregarJugador(krilin)
	var listaGuerreros = torneo.funny()
	assert.notThat(listaGuerreros.contains(krilin))
}